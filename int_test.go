package atomics_test

import (
	"testing"

	"codeberg.org/gruf/go-atomics"
)

func TestInt32Add(t *testing.T) {
	for _, test := range Int32Tests {
		i := atomics.NewInt32()

		if (i.Add(test.V1) != test.V1) ||
			i.Add(test.V2) != test.V1+test.V2 {
			t.Fatal("failed testing .Add")
		}
	}
}

func TestInt64Add(t *testing.T) {
	for _, test := range Int64Tests {
		i := atomics.NewInt64()

		if (i.Add(test.V1) != test.V1) ||
			i.Add(test.V2) != test.V1+test.V2 {
			t.Fatal("failed testing .Add")
		}
	}
}
