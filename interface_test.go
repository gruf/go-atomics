package atomics_test

import (
	"testing"

	"codeberg.org/gruf/go-atomics"
)

func TestInterfaceStoreLoad(t *testing.T) {
	for _, test := range InterfaceTests {
		val := atomics.NewInterface()

		val.Store(test.V1)

		if !(val.Load() == test.V1) {
			t.Fatalf("failed testing .Store and .Load: expect=%v actual=%v", val.Load(), test.V1)
		}

		val.Store(test.V2)

		if !(val.Load() == test.V2) {
			t.Fatalf("failed testing .Store and .Load: expect=%v actual=%v", val.Load(), test.V2)
		}
	}
}

func TestInterfaceCAS(t *testing.T) {
	for _, test := range InterfaceTests {
		val := atomics.NewInterface()

		val.Store(test.V1)

		if val.CAS(test.V2, test.V1) {
			t.Fatalf("failed testing negative .CAS: test=%+v state=%v", test, val.Load())
		}

		if !val.CAS(test.V1, test.V2) {
			t.Fatalf("failed testing positive .CAS: test=%+v state=%v", test, val.Load())
		}
	}
}

func TestInterfaceSwap(t *testing.T) {
	for _, test := range InterfaceTests {
		val := atomics.NewInterface()

		val.Store(test.V1)

		if !(val.Swap(test.V2) == test.V1) {
			t.Fatal("failed testing .Swap")
		}

		if !(val.Swap(test.V1) == test.V2) {
			t.Fatal("failed testing .Swap")
		}
	}
}
