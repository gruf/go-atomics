module codeberg.org/gruf/go-atomics

go 1.16

require (
	codeberg.org/gruf/go-bitutil v1.0.0
	codeberg.org/gruf/go-errors/v2 v2.0.0
)
