package atomics_test

import (
	"testing"

	"codeberg.org/gruf/go-atomics"
	"codeberg.org/gruf/go-bitutil"
)

func TestFlags32Get(t *testing.T) {
	for i := uint8(0); i < 32; i++ {
		flags := atomics.NewFlags32()

		flags.Store(flags.Load().Set(i))

		if !flags.Get(i) {
			t.Fatal("failed testing positive .Get")
		}

		flags.Store(flags.Load().Unset(i))

		if flags.Get(i) {
			t.Fatal("failed testing negative .Get")
		}
	}
}

func TestFlags32Set(t *testing.T) {
	for i := uint8(0); i < 32; i++ {
		flags := atomics.NewFlags32()

		f := flags.Load()

		if !flags.Set(i) {
			t.Fatal("failed testing .Set compare-and-swap")
		} else if flags.Load() != f.Set(i) {
			t.Fatal("failed testing .Set value")
		}
	}
}

func TestFlags32Unset(t *testing.T) {
	for i := uint8(0); i < 32; i++ {
		flags := atomics.NewFlags32()
		flags.Store(^bitutil.Flags32(0))

		f := flags.Load()

		if !flags.Unset(i) {
			t.Fatal("failed testing .Unset compare-and-swap")
		} else if flags.Load() != f.Unset(i) {
			t.Fatal("failed testing .Unset value")
		}
	}
}

func TestFlags64Get(t *testing.T) {
	var flags atomics.Flags64

	for i := uint8(0); i < 64; i++ {
		flags.Store(flags.Load().Set(i))

		if !flags.Get(i) {
			t.Fatal("failed testing positive .Get")
		}

		flags.Store(flags.Load().Unset(i))

		if flags.Get(i) {
			t.Fatal("failed testing negative .Get")
		}
	}
}

func TestFlags64Set(t *testing.T) {
	for i := uint8(0); i < 64; i++ {
		flags := atomics.NewFlags64()

		f := flags.Load()

		if !flags.Set(i) {
			t.Fatal("failed testing .Set compare-and-swap")
		} else if flags.Load() != f.Set(i) {
			t.Fatal("failed testing .Set value")
		}
	}
}

func TestFlags64Unset(t *testing.T) {
	for i := uint8(0); i < 64; i++ {
		flags := atomics.NewFlags64()
		flags.Store(^bitutil.Flags64(0))

		f := flags.Load()

		if !flags.Unset(i) {
			t.Fatal("failed testing .Unset compare-and-swap")
		} else if flags.Load() != f.Unset(i) {
			t.Fatal("failed testing .Unset value")
		}
	}
}
