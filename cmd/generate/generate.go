package main

import (
	"bytes"
	"fmt"
	"os"
	"os/exec"
	"strings"
	"text/template"

	"codeberg.org/gruf/go-errors/v2"
)

type Type struct {
	// Name contains the atomics package type name of this Type.
	Name string

	// Type contains the actual golang type string of this Type.
	Type string

	// Compare provides a boolean comparison of 2 variable names.
	Compare func(string, string) string

	// OnlyTests signifies the type itself doesn't need implementing.
	OnlyTests bool
}

func main() {
	// Load main atomics template file
	atpl, err := template.ParseFiles("atomic.tpl")
	if err != nil {
		panic(err)
	}

	// Load atomics testing template file
	ttpl, err := template.ParseFiles("atomic_test.tpl")
	if err != nil {
		panic(err)
	}

	for _, atype := range []Type{
		// Define any tests required generating below (types manually written):
		{Name: "Int32", Type: "int32", Compare: regularcmp, OnlyTests: true},
		{Name: "Int64", Type: "int64", Compare: regularcmp, OnlyTests: true},
		{Name: "Uint32", Type: "uint32", Compare: regularcmp, OnlyTests: true},
		{Name: "Uint64", Type: "uint64", Compare: regularcmp, OnlyTests: true},
		{Name: "Bool", Type: "bool", Compare: regularcmp, OnlyTests: true},
		{Name: "Flags32", Type: "bitutil.Flags32", Compare: regularcmp, OnlyTests: true},
		{Name: "Flags64", Type: "bitutil.Flags64", Compare: regularcmp, OnlyTests: true},

		// Define atomic types with tests to be generated below:
		{Name: "String", Type: "string", Compare: regularcmp},
		{Name: "Bytes", Type: "[]byte", Compare: stringcmp},
		{Name: "Interface", Type: "interface{}", Compare: regularcmp},
		{Name: "Error", Type: "error", Compare: regularcmp},
		{Name: "Time", Type: "time.Time", Compare: equalcmp},
	} {
		name := strings.ToLower(atype.Name)

		if !atype.OnlyTests {
			if err := generate(atpl, name+".go", atype); err != nil {
				panic(err)
			}
		}

		if err := generate(ttpl, name+"_test.go", atype); err != nil {
			panic(err)
		}
	}
}

func generate(tpl *template.Template, filename string, t Type) error {
	var buf bytes.Buffer

	// Execute the template for atomics type
	if err := tpl.Execute(&buf, t); err != nil {
		return errors.Wrap(err, "error executing template for '"+filename+"'")
	}

	// Open file for the atomic type
	file, err := os.OpenFile(filename, os.O_CREATE|os.O_TRUNC|os.O_WRONLY, 0o644)
	if err != nil {
		return errors.Wrap(err, "error opening file '"+filename+"'")
	}

	// Write generated buffer contents to file
	if _, err := buf.WriteTo(file); err != nil {
		return errors.Wrap(err, "error writing to file '"+filename+"'")
	} else if err := file.Close(); err != nil {
		return errors.Wrap(err, "error closing file '"+filename+"'")
	}

	// Run gofmt on generated file
	cmd := exec.Command("gofumports", "-w", filename)
	if err := cmd.Run(); err != nil {
		return errors.Wrap(err, "error running gofmt -w "+filename)
	}

	return nil
}

func regularcmp(s1, s2 string) string {
	return fmt.Sprintf("%s == %s", s1, s2)
}

func stringcmp(s1, s2 string) string {
	return fmt.Sprintf("string(%s) == string(%s)", s1, s2)
}

func equalcmp(s1, s2 string) string {
	return fmt.Sprintf("%s.Equal(%s)", s1, s2)
}
