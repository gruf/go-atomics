package atomics_test

import (
	"testing"

	"codeberg.org/gruf/go-atomics"
)

func TestUint32Add(t *testing.T) {
	for _, test := range Uint32Tests {
		i := atomics.NewUint32()

		if (i.Add(test.V1) != test.V1) ||
			i.Add(test.V2) != test.V1+test.V2 {
			t.Fatal("failed testing .Add")
		}
	}
}

func TestUint64Add(t *testing.T) {
	for _, test := range Uint64Tests {
		i := atomics.NewUint64()

		if (i.Add(test.V1) != test.V1) ||
			i.Add(test.V2) != test.V1+test.V2 {
			t.Fatal("failed testing .Add")
		}
	}
}
