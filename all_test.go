package atomics_test

import (
	"errors"
	"time"

	"codeberg.org/gruf/go-bitutil"
)

var (
	BoolTests = []struct{ V1, V2 bool }{
		{V1: true, V2: false},
		{V1: false, V2: true},
	}

	BytesTests = []struct{ V1, V2 []byte }{
		{V1: []byte("hello"), V2: []byte("world")},
		{V1: []byte("yes"), V2: []byte("no")},
		{V1: []byte("true"), V2: []byte("false")},
		{V1: []byte("yin"), V2: []byte("yang")},
	}

	ErrorTests = []struct{ V1, V2 error }{
		{V1: errors.New("oh no"), V2: errors.New("uh oh")},
		{V1: errors.New("uh oh"), V2: errors.New("oh no")},
		{V1: errors.New("hello"), V2: errors.New("world")},
		{V1: errors.New("world"), V2: errors.New("hello")},
	}

	Flags32Tests = []struct{ V1, V2 bitutil.Flags32 }{
		{V1: 1, V2: 11},
		{V1: 2, V2: 12},
		{V1: 3, V2: 13},
		{V1: 4, V2: 14},
	}

	Flags64Tests = []struct{ V1, V2 bitutil.Flags64 }{
		{V1: 1, V2: 11},
		{V1: 2, V2: 12},
		{V1: 3, V2: 13},
		{V1: 4, V2: 14},
	}

	Int32Tests = []struct{ V1, V2 int32 }{
		{V1: 1, V2: -1},
		{V1: 2, V2: -2},
		{V1: 3, V2: -3},
		{V1: 4, V2: -4},
	}

	Int64Tests = []struct{ V1, V2 int64 }{
		{V1: 1, V2: -1},
		{V1: 2, V2: -2},
		{V1: 3, V2: -3},
		{V1: 4, V2: -4},
	}

	InterfaceTests = []struct{ V1, V2 interface{} }{
		{V1: "hello", V2: 'h'},
		{V1: time.Now(), V2: 2},
		{V1: true, V2: "false"},
		{V1: "uh oh", V2: errors.New("oh no")},
	}

	StringTests = []struct{ V1, V2 string }{
		{V1: "hello", V2: "world"},
		{V1: "yes", V2: "no"},
		{V1: "true", V2: "false"},
		{V1: "yin", V2: "yang"},
	}

	TimeTests = []struct{ V1, V2 time.Time }{
		{V1: time.Now(), V2: time.Now().Add(time.Second)},
		{V1: time.Now().Add(time.Minute), V2: time.Now()},
		{V1: time.Now(), V2: time.Now().Add(time.Hour)},
		{V1: time.Now().Add(time.Nanosecond), V2: time.Now()},
	}

	Uint32Tests = []struct{ V1, V2 uint32 }{
		{V1: 1, V2: 11},
		{V1: 2, V2: 12},
		{V1: 3, V2: 13},
		{V1: 4, V2: 14},
	}

	Uint64Tests = []struct{ V1, V2 uint64 }{
		{V1: 1, V2: 11},
		{V1: 2, V2: 12},
		{V1: 3, V2: 13},
		{V1: 4, V2: 14},
	}
)
