package atomics_test

import (
	"testing"
	"time"

	"codeberg.org/gruf/go-atomics"
)

func TestStateWithLock(t *testing.T) {
	var st atomics.State

	lock1 := make(chan struct{})

	// Start first lock goroutine
	go st.WithLock(func() {
		lock1 <- struct{}{}
		<-lock1
	})

	// Wait on lock1
	<-lock1

	lock2 := make(chan struct{})

	// Start second lock goroutine
	go st.WithLock(func() {
		lock2 <- struct{}{}
		<-lock2
	})

	select {
	case <-lock2:
		t.Fatal("lock2 succeeded before lock1 return")
	case <-time.After(10 * time.Millisecond):
	}

	// Trigger lock1 unlock
	close(lock1)

	select {
	case <-lock2:
	case <-time.After(10 * time.Millisecond):
		t.Fatal("lock2 failed after lock1 return")
	}

	// Trigger lock2 unlock
	close(lock2)
}

func TestStateStoreLoad(t *testing.T) {
	var st atomics.State

	done := make(chan struct{})

	lockch := make(chan struct{})
	unlockch := make(chan struct{})

	lock := func() { lockch <- struct{}{} }
	unlock := func() { unlockch <- struct{}{} }

	go func() {
		for {
			select {
			case <-done:
				return
			case <-lockch:
			}
			st.WithLock(func() {
				<-unlockch
			})
		}
	}()

	for i := uint32(0); i < 100; i++ {
		// Lock state
		lock()

		storech := make(chan struct{})

		// Attempt store
		go func() {
			st.Store(i)
			<-storech
		}()

		select {
		case <-storech:
			t.Error("failed testing .Store mutex lock")
		case <-time.After(10 * time.Millisecond):
		}

		loadch := make(chan struct{})

		// Attempt load
		go func() {
			st.Load()
			<-loadch
		}()

		select {
		case <-loadch:
			t.Error("failed testing .Store mutex lock")
		case <-time.After(10 * time.Millisecond):
		}

		// Release all
		unlock()
		close(storech)
		close(loadch)

		// Test store/load values
		st.Store(i)
		if st.Load() != i {
			t.Error("failed testing .Store then .Load")
		}
	}
}

func TestStateUpdate(t *testing.T) {
	var st atomics.State

	var prev uint32

	for i := uint32(0); i < 100; i++ {
		next := i

		updch := make(chan struct{})

		// Start an update goroutine
		go st.Update(func(state uint32) uint32 {
			updch <- struct{}{}
			if state != prev {
				t.Error("failed confirming expected state in .Update")
			}
			<-updch
			return next
		})

		// Wait on update
		<-updch

		lockch := make(chan struct{})

		// Attempt simultaneous lock
		go st.WithLock(func() {
			lockch <- struct{}{}
			<-lockch
		})

		select {
		case <-lockch:
			t.Error("failed confirming mutex lock in .Update")
		case <-time.After(10 * time.Millisecond):
		}

		// Trigger update unlock
		close(updch)

		select {
		case <-lockch:
		case <-time.After(10 * time.Millisecond):
			t.Error("failed confirming mutex unlock in .Update")
		}

		// Trigger unlock
		close(lockch)

		prev = next
	}
}

func TestStateCAS(t *testing.T) {
	var st atomics.State

	for i := uint32(0); i < 100; i++ {
		for j := i + 1; j < 100; j++ {
			// Set expected state
			st.Store(i)

			casch := make(chan struct{})

			// Attempt CAS operation
			go st.CAS(i, j, func() {
				casch <- struct{}{}
				<-casch
			})

			// Wait on CAS
			select {
			case <-casch:
			case <-time.After(10 * time.Millisecond):
				t.Error("failed testing .CAS success")
			}

			lockch := make(chan struct{})

			// Attempt simultaneous lock
			go st.WithLock(func() {
				lockch <- struct{}{}
				<-lockch
			})

			select {
			case <-lockch:
				t.Error("failed confirming mutex lock in .CAS")
			case <-time.After(10 * time.Millisecond):
			}

			// Trigger CAS unlock
			close(casch)

			select {
			case <-lockch:
			case <-time.After(10 * time.Millisecond):
				t.Error("failed confirming mutex unlock in .CAS")
			}

			// Trigger unlock
			close(lockch)

			// Perform negative CAS test
			if st.CAS(i, j, func() {}) {
				t.Errorf("failed testing .CAS failure: state=%d i=%d j=%d", st.Load(), i, j)
			}
		}
	}
}
